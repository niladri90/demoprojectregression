package com.demoproject.test;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Methodrepository {

	@BeforeMethod
	
	public void callLaunching() {
		
		Driver.appLaunch();
		
	}
	
	@Test(priority=1)
	
	public void dropdownSelection() throws IOException, AWTException, InterruptedException, FindFailed {
		
		Driver.choosePaymentplan();
		
	}
	
	
	
	@AfterMethod
	
	public void closedriver() throws InterruptedException {
		Driver.driverclose();
	}
	
}
