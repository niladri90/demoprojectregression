package com.demoproject.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class Driver {

	static WebDriver driver;

	public static void appLaunch() {
		// driver

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("https://www.freecrm.com/register/");

	}

	public static void choosePaymentplan() throws IOException, AWTException, InterruptedException, FindFailed {

		String expectedvalue = "Free Edition";
		WebElement dropdown = driver.findElement(By.xpath("//select[@id='payment_plan_id']"));
		Select chooseplan = new Select(dropdown);
		chooseplan.selectByVisibleText("Free Edition");

		List<WebElement> oSize = chooseplan.getOptions();
		int iListSize = oSize.size();

		// Setting up the loop to print all the options
		for (int i = 0; i < iListSize; i++) {
			// Storing the value of the option
			String sValue = chooseplan.getOptions().get(i).getText();
			// Printing the stored value
			if (i == 1) {
				String actualvalue = "Free Edition";
				System.out.println(actualvalue);

				if (expectedvalue.equals(actualvalue)) {

					System.out.println("Test case is passed");
				} else {

					System.out.println("Test case is failed");
				}
			}
		}
		// Using Properties file send keys
		WebElement firstname = driver.findElement(By.xpath("//input[@placeholder='First Name']"));
		WebElement lastname = driver.findElement(By.xpath("//input[@placeholder='Last Name']"));
		Properties obj = new Properties();
		FileInputStream objfile = new FileInputStream("./dataset/datasets.properties");
		obj.load(objfile);
		String namefirst = obj.getProperty("Firstname");
		firstname.sendKeys(namefirst);

		String namelast = obj.getProperty("Lastname");
		lastname.sendKeys(namelast);

		// Using JavascriptExecutor

		Thread.sleep(5000);

		WebElement email = driver.findElement(By.xpath("//input[@placeholder='Email']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='freecrm106@mailinator.com';", email);

		// Using Action Class

		WebElement confirmemail = driver.findElement(By.xpath("//input[@placeholder='Confirm Email']"));
		Actions emailconfirmation = new Actions(driver);
		emailconfirmation.moveToElement(confirmemail).click().sendKeys("freecrm106@mailinator.com").build().perform();

		// Using Sikuli

		Screen screen = new Screen();
		Pattern username = new Pattern("./images/Capture_username.png");
		screen.wait(username, 10);
		screen.type(username, "freecrm106");

		// Using Robot class
		WebElement pwd = driver.findElement(By.xpath("//input[@placeholder='Password']"));
		pwd.sendKeys("");

		String password = "123456";
		StringSelection passwordelection = new StringSelection(password);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(passwordelection, passwordelection);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		// Using Robot Class with entering password one by one.

		robot.keyPress(KeyEvent.VK_TAB);// Press the field with tab
		robot.keyRelease(KeyEvent.VK_TAB);

		robot.keyPress(KeyEvent.VK_1);
		robot.keyRelease(KeyEvent.VK_1);
		robot.keyPress(KeyEvent.VK_2);
		robot.keyRelease(KeyEvent.VK_2);
		robot.keyPress(KeyEvent.VK_3);
		robot.keyRelease(KeyEvent.VK_3);
		robot.keyPress(KeyEvent.VK_4);
		robot.keyRelease(KeyEvent.VK_4);
		robot.keyPress(KeyEvent.VK_5);
		robot.keyRelease(KeyEvent.VK_5);
		robot.keyPress(KeyEvent.VK_6);
		robot.keyRelease(KeyEvent.VK_6);

		// using sikuli click on the check box

		screen.find("./images/checkbox.png");
		screen.click();

		// Screen scroll using JavascriptExecutor till the submit button found

		WebElement btnsubmit = driver.findElement(By.id("submitButton"));

		// This will scroll the page till the element is found
		js.executeScript("arguments[0].scrollIntoView();", btnsubmit);

		Thread.sleep(2000);
		// using robot class tap on the submit button

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		// check page title for the company details

		String expectedtitle = "CRMPRO - CRM Pro for customer relationship management, sales, and support";
		String actualtitle = driver.getTitle();
		System.out.println(actualtitle);

		if (expectedtitle.equals(actualtitle)) {

			System.out.println("Test Case is passed");
		} else {

			System.out.println("Test Case is failed");
		}
         //abc
		// Enter company name using properties file

		WebElement companyname = driver.findElement(By.id("company_name"));
		String company = obj.getProperty("Company");
		companyname.sendKeys(company);

	}

	public static void driverclose() throws InterruptedException {

		Thread.sleep(20000);

		driver.quit();
	}

}
